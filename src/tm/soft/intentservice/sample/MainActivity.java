package tm.soft.intentservice.sample;

import tm.lib.multithreadservice.MultiThreadIntentService;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(R.id.container, new PlaceholderFragment()).commit();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		stopService(new Intent(this, MTIntentServiceSample.class));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment implements OnClickListener {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container, false);
			rootView.findViewById(R.id.button1).setOnClickListener(this);
			rootView.findViewById(R.id.button2).setOnClickListener(this);
			rootView.findViewById(R.id.button3).setOnClickListener(this);
			return rootView;
		}

		private void test(int[] action) {
			for (int i = 0; i < 10; i++) {
				int pos = i % action.length;
				Intent service = new Intent(getActivity(), MTIntentServiceSample.class);
				service.putExtra(MultiThreadIntentService.INTENT_PARAM_THREAD_TYPE, pos);
				service.putExtra(MTIntentServiceSample.ACTION, action[pos]);
				getActivity().startService(service);
			}

		}

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.button1:
				test(new int[] { MTIntentServiceSample.ACTION_ONE });
				break;
			case R.id.button2:
				test(new int[] { MTIntentServiceSample.ACTION_ONE, MTIntentServiceSample.ACTION_TWO });
				break;
			case R.id.button3:
				test(new int[] { MTIntentServiceSample.ACTION_ONE, MTIntentServiceSample.ACTION_TWO,
						MTIntentServiceSample.ACTION_THREE });
				break;
			}
		}
	}

}
