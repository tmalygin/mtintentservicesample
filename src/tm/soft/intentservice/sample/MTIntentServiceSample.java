/**
 * 
 */
package tm.soft.intentservice.sample;

import android.content.Intent;
import android.util.Log;
import tm.lib.multithreadservice.MultiThreadIntentService;

/**
 * @author TMalygin
 * 
 */
public class MTIntentServiceSample extends MultiThreadIntentService {

	public static final String ACTION = "action";

	private static final String TAG = "MTSample ";

	public static final int ACTION_ONE = 0;
	public static final int ACTION_TWO = 1;
	public static final int ACTION_THREE = 2;

	/**
	 * @param name
	 * @param sizeThreads
	 */
	public MTIntentServiceSample() {
		super(TAG, 3);
	}

	private void actionOne() {
		Log.v("", TAG + "start actionOne");
		try {
			// Thread.sleep(3000);
		} catch (Exception e) {
			Log.e("", TAG + "error actionOne", e);
		}
		Log.v("", TAG + "ends actionOne");
	}

	private void actionTwo() {
		Log.v("", TAG + "start actionTwo");
		try {
			Thread.sleep(6000);
		} catch (Exception e) {
			Log.e("", TAG + "error actionTwo", e);
		}
		Log.v("", TAG + "ends actionTwo");
	}

	private void actionThree() {
		Log.v("", TAG + "start actionThree");
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			Log.e("", TAG + "error actionThree", e);
		}
		Log.v("", TAG + "ends actionThree");
	}

	/**
	 */
	@Override
	public void onHandleIntent(int num, Intent intent) {
		int action = intent.getIntExtra(ACTION, -1);
		Log.v("", TAG + action);
		switch (action) {
		case ACTION_ONE:
			actionOne();
			break;
		case ACTION_TWO:
			actionTwo();
			break;
		case ACTION_THREE:
			actionThree();
			break;
		}

	}
}
